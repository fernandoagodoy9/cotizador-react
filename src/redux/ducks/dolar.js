//ation types
const ADD_RESULT = 'dolar/ADD_RESULT'

const initialState = {
    items: [{
        id:1,
        date: '01/01/19',
        cotizacion: 0,
        result: 0
    }]
}

// reducer
export default function reducer(state = initialState, action){
    console.log(action.type);
    switch(action.type){
        case ADD_RESULT:
            return {
                ...state,
                items: [...state.items, action.history]
            }
        default: 
            return state
    }
}

// action creators
export const addToHistory = (history) => ({
    type: ADD_RESULT,
    history
})