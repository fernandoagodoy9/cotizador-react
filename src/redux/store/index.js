import  { createStore, combineReducers } from 'redux'
import dolarReducer from '../ducks/dolar'

const rootReducer = combineReducers({
    dolar: dolarReducer
})

export default createStore(rootReducer, {})