import React from 'react'
import './App.css'
import Header from './components/Header'
import CotizSearch from './components/CotizSearch'

class App extends React.Component {
	render(){
		return (
		    <div className="App">
		      <Header />
		      <CotizSearch />
		    </div>
		  );		
	}
  
}

export default App;
