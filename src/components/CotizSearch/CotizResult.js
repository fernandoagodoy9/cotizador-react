import React from 'react';

const CotizResult = ({ data }) => (
	<div>
		<h1>{ data.nombre }</h1>
		<div>Venta: {data.venta} - Compra: {data.compra}</div>
	</div>
)

export default CotizResult;