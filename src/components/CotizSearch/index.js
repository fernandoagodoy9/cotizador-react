import React from 'react';
import axios from 'axios';
import CotizResult from './CotizResult';
import CotizCalc from '../../components/CotizCalc'

class CotizSearch extends React.Component{

	constructor(){
		super();
		this.state = {
			bank: [],
			price: 0
		};
	}

	onSearch = async() => {
		try{const url = 'https://www.dolarsi.com/api/api.php?type=capital';
			const response = await axios.get(url);
			this.setState({
				bank: response.data[1].casa
			});
			console.log(response.data[1]);
		} catch (err){
			console.log(err);
		}
	}

	componentWillMount(){
	 	this.onSearch();
	}
	render(){
		return(
			<div>
				<CotizResult data={this.state.bank}/>
				<CotizCalc data={this.state.bank} />
			</div>
		);
	}

}

export default CotizSearch;