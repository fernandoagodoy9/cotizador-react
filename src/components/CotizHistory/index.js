import React from 'react'
import { connect } from 'react-redux'


class CotizHistory extends React.Component{
	render(){
		const { history } = this.props
		const groupedItems = []
		for (const item of history) {
	      const foundItem = groupedItems.find(x => x.id === item.id)
	      if (foundItem) {
	        foundItem.quantity++
	      } else {
	        groupedItems.push({ ...item, quantity: 1 })
	      }
	    }
	    const liResult = groupedItems.map(row => (
	    	<li key={row.id}><b>Fecha:</b> {row.date} <b>Cotizacion:</b> {row.cotizacion} <b>Resultado:</b> {row.result}</li>
	    ))
		console.log(history);
		return (
			<ul>
				{liResult}
			</ul>
			);
	}
}

const mapStateToProps = state => ({
  history: state.dolar.items
})

export default connect(mapStateToProps)(CotizHistory)