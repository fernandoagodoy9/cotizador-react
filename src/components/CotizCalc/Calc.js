import React from 'react'

const Calc = ({ onChange, onCalcular,calcCotiz }) => (
	<div>
		<input type="text" onChange={onChange} value={calcCotiz} placeholder="Ingrese el monto" />
		<button onClick={onCalcular} >Calcular!</button>
	</div>
)

export default Calc