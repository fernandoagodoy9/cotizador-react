import React from 'react';
import Calc from './Calc';
import { addToHistory } from '../../redux/ducks/dolar'
import { connect } from 'react-redux'
import CotizHistory from '../../components/CotizHistory'

class CotizCalc extends React.Component{
	constructor () {
		super();
		this.state = {
			resultTemp: 0,
			total: 0
		}

	}

	onChange = (e) => {
		const value = e.currentTarget.value;
		this.setState({
			resultTemp: value
		})
	}
	onCalcular = () =>{
		const { data } = this.props;
		const dolar = data.venta.replace(",",".");
		const res = 1/dolar * this.state.resultTemp;
		this.setState({
			total: res.toFixed(2)
		});

		const history = {
	        id:1,
	        date: '28/06/19',
	        cotizacion: dolar,
	        result: res.toFixed(2)
	    }
		addToHistory(history)
	}

	render () {
		const { addToHistory } = this.props;
		return (
			<div>
				<Calc onChange={this.onChange} onCalcular={this.onCalcular} calcCotiz={this.state.resultTemp}/>
				<h3>{this.state.total}</h3>
				<CotizHistory />
			</div>
		)
	}
}
const mapDispatchToProps = {
  addToHistory
}
export default connect(null, mapDispatchToProps)(CotizCalc)